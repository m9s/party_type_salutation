# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Salutation',
    'name_de_DE': 'Parteien Briefanrede',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    Provides the possibility to define salutations depending
    - on the gender of persons
    - on the type of parties
    ''',
    'description_de_DE': '''
    Ermöglicht die Definition von Briefanreden abhängig von
    - dem Geschlecht von Personen
    - dem Typ von Parteien
    ''',
    'depends': [
        'party_type',
    ],
    'xml': [
        'salutation.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
